﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppTest.Controllers
{
    [Route("/api/[controller]")]
    public class TestController : Controller
    {
        [HttpGet("/ping")]
        public IActionResult Ping()
        {
            return Ok("Pong");
        }
    }
}
